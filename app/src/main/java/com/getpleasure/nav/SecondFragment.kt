package com.getpleasure.nav

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class SecondFragment : Fragment(), ExitWithAnimation {

	override var posX: Int? = null
	override var posY: Int? = null

	override fun isToBeExitedWithAnimation(): Boolean = true

	companion object {
		@JvmStatic
		fun newInstance(exitPoints: IntArray? = null): SecondFragment = SecondFragment().apply {
			if (exitPoints != null && exitPoints.size == 2) {
				posX = exitPoints[0]
				posY = exitPoints[1]
			}
		}
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.second_fragment, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			view.openWithCircularRevealAnimation(fromLeft = true, fromTop = true)
		}
	}
}
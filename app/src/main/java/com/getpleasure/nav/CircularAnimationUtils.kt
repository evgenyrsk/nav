package com.getpleasure.nav

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Build
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.DecelerateInterpolator
import androidx.annotation.RequiresApi

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.openWithCircularRevealAnimation(fromLeft: Boolean, fromTop: Boolean) {
	addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
		override fun onLayoutChange(
			v: View,
			left: Int,
			top: Int,
			right: Int,
			bottom: Int,
			oldLeft: Int,
			oldTop: Int,
			oldRight: Int,
			oldBottom: Int
		) {
			v.removeOnLayoutChangeListener(this)

			val xFrom = if (fromLeft) v.left else v.right
			val yFrom = if (fromTop) v.top else v.bottom
			val radius = Math.hypot(right.toDouble(), bottom.toDouble()).toInt()

			ViewAnimationUtils.createCircularReveal(v, xFrom, yFrom, 0f, radius.toFloat())
				.apply {
					interpolator = DecelerateInterpolator(2.5f)
					duration = 1000
					start()
				}
		}
	})
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.exitWithCircularRevealAnimation(exitX: Int, exitY: Int, block: () -> Unit) {
	val startRadius = Math.hypot(this.width.toDouble(), this.height.toDouble())
	ViewAnimationUtils.createCircularReveal(this, exitX, exitY, startRadius.toFloat(), 0f)
		.apply {
			duration = 350
			interpolator = DecelerateInterpolator(1f)
			addListener(object : AnimatorListenerAdapter() {
				override fun onAnimationEnd(animation: Animator?) {
					block()
					super.onAnimationEnd(animation)
				}
			})
			start()
		}
}

fun View.findLocationOfCenterOnTheScreen(): IntArray {
	val positions = intArrayOf(0, 0)
	getLocationInWindow(positions)    // Get the center of the view
	positions[0] = positions[0] + width / 2
	positions[1] = positions[1] + height / 2
	return positions
}

/**
 * Интерфейс следует реализовать у фрагментов, которые хочется открывать/закрывать с использованием
 * circular reveal анимации.
 * @property posX позиция центра анимации по оси X
 * @property posY позиция центра анимации по оси Y
 */
interface ExitWithAnimation {
	var posX: Int?
	var posY: Int?
	/**
	 * Возвращать true, если закрыть нужно тоже с анимацией.
	 */
	fun isToBeExitedWithAnimation(): Boolean
}
package com.getpleasure.nav

import android.os.Build
import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

	private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
		when (item.itemId) {
			R.id.navigation_home -> {
				openFragment(MenuSection.ONE)
				return@OnNavigationItemSelectedListener true
			}
			R.id.navigation_dashboard -> {
				return@OnNavigationItemSelectedListener true
			}
			R.id.navigation_notifications -> {
				openFragment(MenuSection.TWO)
				return@OnNavigationItemSelectedListener true
			}
		}
		false
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		val navView: BottomNavigationView = findViewById(R.id.nav_view)
		navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

		openFragment(MenuSection.ONE)
	}

	override fun onBackPressed() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			super.onBackPressed()
			return
		}
		with(supportFragmentManager.findFragmentById(R.id.container)) {
			if ((this as? ExitWithAnimation)?.isToBeExitedWithAnimation() == true) {
				if (this.posX == null || this.posY == null) {
					super.onBackPressed()
				} else {
					this.view?.exitWithCircularRevealAnimation(
						this.posX!!,
						this.posY!!,
						block = { super.onBackPressed() }
					)
				}
			} else {
				super.onBackPressed()
			}
		}
	}

	private fun openFragment(section: MenuSection) {
		val fragment: Fragment
		val positions = findViewById<FrameLayout>(R.id.container)
			.rootView
			.findLocationOfCenterOnTheScreen()
		when (section) {
			MenuSection.ONE -> {
				fragment = FirstFragment.newInstance(positions)
			}
			MenuSection.TWO -> {
				fragment = SecondFragment.newInstance(positions)
			}
		}
		supportFragmentManager.beginTransaction()
			.add(R.id.container, fragment)
			.addToBackStack(null)
			.commit()
	}
}
